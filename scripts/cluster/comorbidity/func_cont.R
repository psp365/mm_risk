library(ggraph)
library(igraph)
library(tidyverse)
library(scales)

plot_cluster_cont <- function(ltc_category, agebin, clusternum, ltclabelsize, chisq_ul){
  #load comorbidity data, computed comorbidity of ltcs within cluster
  df <- read.csv(paste(agebin,"/","cluster_",as.character(clusternum),".csv",sep=""), stringsAsFactors = FALSE)
  
  #map ltc condition to its category
  df<-list(df, ltc_category[,c("ltc","Category_short")]) %>% reduce(left_join, by="ltc")
  df[12,"Category_short"]<-"MTB" #some ltc ar NA. assign category
  df[20,"Category_short"]<-"G"
  df[19,"Category_short"]<-"G"
  
  #build hierarchy df. 
  #maps origin -> system; system -> ltc
  d1 <- data.frame(from="origin", to=df$Category_short)
  d2 <- data.frame(from=d1$to, to=df$ltc)
  hierarchy <- rbind(d1, d2)
  
  #build vertices df. this has info about the nodes. each ltc is a node
  #vertices$name is list of oriting, systems, ltcs
  vertices <- data.frame(name = unique(c(as.character(hierarchy$from), as.character(hierarchy$to))) )
  #vertices$group is what level each var belongs to. eg AF group is CIR
  vertices$group  <-  hierarchy$from[ match( vertices$name, hierarchy$to ) ]
  
  #we also need the ltc prevalence and OE features:
  vertices$ltc<-as.character(vertices$name) #easier for merging
  vertices <- list(vertices, df[,c("ltc",paste("Cluster_",as.character(clusternum),"OE",sep=""),
                                   paste("Cluster_",as.character(clusternum),"prevalence",sep=""))]) %>%
    reduce(left_join, by="ltc") #get oe and prevalence via merge on ltc name
  #some (categories) will be NA. just set to 1
  vertices$oe<-vertices[,paste("Cluster_",as.character(clusternum),"OE",sep="")]
  vertices$prevalence<-vertices[,paste("Cluster_",as.character(clusternum),"prevalence",sep="")]
  
  vertices$oe[is.na(vertices[,paste("Cluster_",as.character(clusternum),"OE",sep="")])]<-1
  vertices$prevalence[is.na(vertices[,paste("Cluster_",as.character(clusternum),"prevalence",sep="")])]<-1
  
  vertices$oe[which(vertices$oe<1)]<-0.9
  
  #we want a consistent scale across all clusters
  #lets make a dummy var with an oe of 500 (really high)
  #then lets scale all values with this maximum in mind
  #if we dont do this, a cluster with low oe will have show its max value (say 1.5) the same as
  #a cluster with higher oes across the board
  dummy_oe<-vertices[,c("name","oe")]
  max_oe<-data.frame("x",50)
  names(max_oe)<-c("name","oe")
  dummy_oe<-rbind(dummy_oe, max_oe)
  dummy_oe$log_oe<-log(dummy_oe$oe)
  #dummy_oe$scaled_log_oe<-(dummy_oe$log_oe - min(dummy_oe$log_oe))/(max(dummy_oe$log_oe) - min(dummy_oe$log_oe))
  dummy_oe$scaled_log_oe<-rescale(dummy_oe$log_oe, to=c(0.5,25))
  vertices<-list(vertices, dummy_oe[,c("name","scaled_log_oe")]) %>% reduce(left_join, by="name")
  
  vertices$size_bin<-ifelse(vertices$oe<=2,"none",
                            ifelse(vertices$oe<=3,"low",
                                   ifelse(vertices$oe<=10,"med",
                                          ifelse(vertices$oe<=25,"high","max"))))
  
  vertices$size_bin<-ifelse(vertices$oe<=2,"none",
                            ifelse(vertices$oe<=3,"low",
                                   ifelse(vertices$oe<=5,"lowmed",
                                          ifelse(vertices$oe<=10,"med",
                                                 ifelse(vertices$oe<=30,"high","max")))))
  
  vertices$size_bin<-as.factor(vertices$size_bin)
  
  myangles<-read.csv("vertices.csv")
  
  vertices<-list(vertices, myangles[,c("ltc","angle","displayname_short")]) %>% reduce(left_join, by="ltc")
  
  # calculate the alignment of labels: right or left
  # If I am on the left part of the plot, my labels have currently an angle < -90
  vertices$hjust <- ifelse( vertices$angle < -90, 1, 0)
  
  # flip angle BY to make them readable
  vertices$angle <- ifelse(vertices$angle < -90, vertices$angle+180, vertices$angle)
  
  
  
  #build a connect df that lists the comorbidity between ltcs
  from_list=list()
  to_list=list()
  value_list=list()
  #traverse upper triangle of df, which has comorbidity data
  #if comorb is above 0.1, store the from and to ltcs
  for (r in seq(1,45)){
    for (c in seq(r+2,47)){
      if(df[r,c]>=0){
        #print(df$ltc[r])
        #print(names(df[c]))
        #print("")
        from_list<-append(from_list, df$ltc[r])
        to_list<-append(to_list, names(df)[c])
        value_list<-append(value_list, df[r,c])
      }
    }
  }
  #make df based on lists built
  connect_data<-list(from=unlist(from_list), to=unlist(to_list), value=unlist(value_list))
  connect<-as.data.frame(connect_data)
  connect <- connect[order(connect$value),]
  
  connect$value_bin<-ifelse(connect$value<2,0,
                            ifelse(connect$value<3,0.1,
                                   ifelse(connect$value<4,0.5,
                                          ifelse(connect$value<6,0.8,0.9))))
  
  v_col <- connect$value_bin
  v_col_map <- ifelse(v_col==0,0.2,0.9)
  v_col_map_hide <- ifelse(v_col==0,0,0.9)

  from <- match( connect$from, vertices$name)
  to <- match( connect$to, vertices$name)

  colourvec<-c("BLD"="#E31A1C", "CIR"="#E31A1C", "EAR"="#1F78B4",
               "END"="#64FF64", "EYE"="#0A0A0A", "G"="#33A02C",
               "GEN"="#008080",  "MH"="#6A3D9A", "MSK"="#646464",
               "MTB"="#FFA500", "NS"="#A6CEE3", "O"="#FF00FF", "R"="#B15928")
  
  connect$value_cont <- ifelse(connect$value>chisq_ul,chisq_ul,connect$value)
  
  prev_palette_inferno <- inferno(801)
  prev_palette_magma <- magma(801)
  link_colours_magma <- c()
  link_colours_inferno <- c()
  for (v in connect$value_cont){
    idx = v*100 + 1
    link_colours_inferno <- append(link_colours_inferno, prev_palette_inferno[idx] )
    link_colours_magma <- append(link_colours_magma, prev_palette_magma[idx] )
  }
  
  
  #create mygraph obj from hierarchy and vertices data
  mygraph <- graph_from_data_frame( hierarchy, vertices=vertices )
  p <- ggraph(mygraph, layout = 'dendrogram', circular = TRUE) + 
    theme_void()
  
  inferno_plot <- p + 
    scale_colour_manual(values= rep( colourvec , 30), guide="none") +
    geom_conn_bundle(data = get_con(from = from, to = to),
                     colour=rep(link_colours_inferno, each=100),
                     alpha=rep(v_col_map, each=100), width=1.5, tension=0.9) +
    geom_node_point(aes(filter = leaf, x = x*1.01, y=y*1.01, colour=group, size=size_bin),alpha=0.5) +
    geom_node_text(aes(x = x*1.15, y=y*1.15, filter = leaf, label=displayname_short, angle = angle, hjust = hjust), size=setlabelsize, alpha=1) +
    scale_size_manual(values=c("none"=1.5,"low"=4,"lowmed"=10,"med"=14,"high"=18,"max"=24), guide="none") +
    #qqqtheme(panel.background = element_rect(fill = 'grey99')) + 
    theme(legend.position="none") + expand_limits(x = c(-1.5, 1.5), y = c(-1.5, 1.5))
  
  # vertices$size_bin<-ifelse(vertices$oe<=2,"none",
  #                           ifelse(vertices$oe<=3,"low",
  #                                  ifelse(vertices$oe<=5,"lowmed",
  #                                         ifelse(vertices$oe<=10,"med",
  #                                                ifelse(vertices$oe<=30,"high","max")))))
  
  inferno_plot_hidden <- p + 
    scale_colour_manual(values= rep( colourvec , 30), guide="none") +
    geom_conn_bundle(data = get_con(from = from, to = to),
                     colour=rep(link_colours_inferno, each=100),
                     alpha=rep(v_col_map_hide, each=100), width=1.5, tension=0.9) +
    geom_node_point(aes(filter = leaf, x = x*1.01, y=y*1.01, colour=group, size=size_bin),alpha=0.5) +
    geom_node_text(aes(x = x*1.15, y=y*1.15, filter = leaf, label=displayname_short, angle = angle, hjust = hjust), size=setlabelsize, alpha=1) +
    scale_size_manual(values=c("none"=1.5,"low"=4,"lowmed"=10,"med"=14,"high"=18,"max"=24), guide="none") +
    #theme(panel.background = element_rect(fill = 'grey99')) + 
    theme(legend.position="none") + expand_limits(x = c(-1.5, 1.5), y = c(-1.5, 1.5))
  
  return(list(inferno_plot, inferno_plot_hidden,mygraph,connect,vertices,hierarchy))
  
}