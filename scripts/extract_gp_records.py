import sys
import csv
from typing import List, Any, Union
import os
import pandas as pd
import numpy as np

gp_split=str(sys.argv[1])
m=str(sys.argv[2])
df_gp_clinical = pd.read_csv('../../raw_data/gp_clinical_'+gp_split+'.txt', delimiter = "\t")
rcodes_list = pd.read_csv('../ltc_readcodes.csv')

#get list of relevant codes
list_to_extract = rcodes_list[m].tolist()
#drop nans
list_to_extract = [x for x in list_to_extract if pd.notnull(x)]
   
#for each read code, assign true/false to each row of gp clinical for read 2 and read 3 columns
#get a df for each read code
#concatenate the output dfs
#get the indices of any that have true in one of the columns
idx = pd.concat((df_gp_clinical[col].astype(str).str.startswith(tuple(list_to_extract)) for col in ['read_2', 'read_3']), axis=1).any(axis=1)
#subset to those identified with a readcode
df_has_dx = df_gp_clinical.loc[idx].copy()
    
print ('saving file containing participants with %s' %m)
    
#create an idx column that contains the count/number of times each subject id is found
df_has_dx['idx'] = df_has_dx.groupby('eid').cumcount() + 1
    
#make a pivot table with 1 row per subject, and 1 col per event
#values of table are event dates, sorted by time, s.t. first col is subj id, 2nd col is date of event 1, 3rd col is date of event 2
df_has_dx = df_has_dx.pivot_table(index=['eid'], columns='idx',
                    values=['event_dt'], aggfunc='first')
print(len(df_has_dx), 'with ', m)
                     
    
#rename columns
df_has_dx = df_has_dx.sort_index(axis=1, level=1)
df_has_dx.columns = [f'{x}_{y}' for x, y in df_has_dx.columns]
df_has_dx = df_has_dx.reset_index()
#save!
df_has_dx.to_csv('../../derivatives/participants_with_%s' %m + '_' + gp_split + '.csv', sep=',',index=None)
#clean
del(df_has_dx, idx)
